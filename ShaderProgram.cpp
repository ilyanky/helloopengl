#include "ShaderProgram.h"



ShaderProgram::ShaderProgram(const int& id) : _id(id)
{
}


ShaderProgram::~ShaderProgram()
{
}

ShaderProgram* ShaderProgram::Init()
{
	int shaderProgram = glCreateProgram();
	if ( shaderProgram == 0 )
		return nullptr;
	auto program = new ShaderProgram(shaderProgram);
	return program;
}

ShaderProgram* ShaderProgram::Init(const Shader* vertexShader, 
	const Shader* fragmentShader, std::string* error)
{
	auto shaderProgram = ShaderProgram::Init();
	if ( shaderProgram == nullptr )
	{
		*error = "can't create shader program";
		return nullptr;
	}
	shaderProgram->AttachShader(vertexShader);
	shaderProgram->AttachShader(fragmentShader);

	if ( !shaderProgram->LinkProgram(error) )
		return nullptr;

	return shaderProgram;
}

ShaderProgram* ShaderProgram::InitAndDeleteShaders(Shader* vertexShader,
	Shader* fragmentShader, std::string* error)
{
	auto prog = ShaderProgram::Init(vertexShader, fragmentShader, error);
	if ( prog == nullptr )
		return nullptr;

	Shader::DeleteShader(vertexShader);
	Shader::DeleteShader(fragmentShader);
	return prog;
}

void ShaderProgram::UseProgram(const ShaderProgram* prog)
{
	glUseProgram(prog->_id);
}

void ShaderProgram::AttachShader(const Shader* shader)
{
	glAttachShader(_id, shader->Id());
	this->_shaders.push_back(shader->Id());
}

bool ShaderProgram::LinkProgram(std::string* error)
{
	glLinkProgram(_id);

	int success;
	glGetProgramiv(_id, GL_LINK_STATUS, &success);
	if ( !success ) 
	{
		char infoLog[512];
		glGetProgramInfoLog(_id, 512, NULL, infoLog);
		*error = "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" + std::string(infoLog);
		return false;
	}

	return true;
}

bool ShaderProgram::SetFloat(const std::string & name, const float & val)
{
	auto location = glGetUniformLocation(this->_id, name.c_str());
	if ( location == -1 )
		return false;
	glUniform1f(location, val);
	return true;
}

bool ShaderProgram::SetInt(const std::string& name, const int& val)
{
	auto location = glGetUniformLocation(this->_id, name.c_str());
	if ( location == -1 )
		return false;
	glUniform1i(location, val);
	return true;
}

int ShaderProgram::Id() const
{
	return this->_id;
}
