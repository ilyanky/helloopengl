#pragma once

#include <glad/glad.h>

#include <string>
#include <fstream>
#include <sstream>


enum ShaderType
{
	VertexShader,
	FragmentShader
};

class Shader
{
private:
	ShaderType _type;
	int _id;


private:
	Shader(const ShaderType& type, int id);
	static std::string readFromFile(const std::string& path, bool* isOk = nullptr);
	static int createShader(const ShaderType& type, bool* isOk = nullptr);

public:
	~Shader();

	static Shader* Compile(const ShaderType& type, const std::string& path, 
		std::string* error = nullptr);
	static void DeleteShader(Shader* shader);

	int Id() const;
};

