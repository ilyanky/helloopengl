#include "Shader.h"

Shader::Shader(const ShaderType& type, int id) : _type(type), _id(id)
{
}



Shader::~Shader()
{
}


std::string Shader::readFromFile(const std::string& path, bool* isOk)
{
	std::ifstream fileStream;
	fileStream.exceptions(std::ifstream::failbit | std::ifstream::badbit);
	try
	{
		fileStream.open(path);
		std::stringstream strStream;
		strStream << fileStream.rdbuf();
		fileStream.close();
		return strStream.str();
	}
	catch ( std::ifstream::failure e )
	{
		*isOk = false;
		return "ERROR::SHADER::FILE_NOT_SUCCESFULLY_READ";
	}
}

int Shader::createShader(const ShaderType& type, bool* isOk)
{
	int res;
	if ( type == ShaderType::VertexShader )
		res = glCreateShader(GL_VERTEX_SHADER);
	else if ( type == ShaderType::FragmentShader )
		res = glCreateShader(GL_FRAGMENT_SHADER);
	else
		res = 0;

	if ( res == 0 )
		*isOk = false;
	return res;
}


Shader* Shader::Compile(const ShaderType& type, const std::string& path, 
	std::string* error)
{
	bool isOk = true;
	auto code = Shader::readFromFile(path, &isOk);
	if ( !isOk )
	{
		*error = code;
		return nullptr;
	}
	
	auto shaderId = Shader::createShader(type, &isOk);
	if ( !isOk )
	{
		*error = "can't create shader";
		return nullptr;
	}


	const char* cStrCode = code.c_str();
	glShaderSource(shaderId, 1, &cStrCode, NULL);
	glCompileShader(shaderId);

	int success;
	glGetShaderiv(shaderId, GL_COMPILE_STATUS, &success);
	if ( !success )
	{
		char infoLog[512];
		glGetShaderInfoLog(shaderId, 512, NULL, infoLog);
		glDeleteShader(shaderId);
		*error = "ERROR::SHADER::COMPILATION_FAILED\n" + std::string(infoLog);
		return nullptr;
	};


	auto shader = new Shader(type, shaderId);
	return shader;
}

void Shader::DeleteShader(Shader* shader)
{
	glDeleteShader(shader->_id);
	delete shader;
}

int Shader::Id() const
{
	return _id;
}

