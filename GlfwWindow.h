#pragma once

#include <iostream>
#include <string>
#include <map>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "ShaderProgram.h"

typedef void(*ProcessKeyDelegate)();



class GlfwWindow
{
private:
	std::map<int, ProcessKeyDelegate> _keyBindings;
	std::string _title;
	int _width;
	int _height;
	GLFWwindow* _window;

private:
	GlfwWindow(GLFWwindow* window);
	static void userResizeWindowHandler(GLFWwindow* window, int width, int height);

public:
	~GlfwWindow();

	static GlfwWindow* Init(const int& width, const int& height, const std::string& title,
		GLFWmonitor* monitor, GLFWwindow* share, std::string* error = nullptr);
	static void SetContextToWnd(GlfwWindow* wnd);
	static void SwapBuffers(const GlfwWindow* window);

	int Width() const;
	int Height() const;
	const GLFWwindow* Window() const;
	bool IsClosing() const;
	void processInput();
	void BindKey(const int& key, ProcessKeyDelegate callback);
};

