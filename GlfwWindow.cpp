#include "GlfwWindow.h"


GlfwWindow::GlfwWindow(GLFWwindow* window) : _window(window)
{
}


GlfwWindow::~GlfwWindow()
{
}





GlfwWindow* GlfwWindow::Init(const int& width, const int& height, const std::string& title, 
	GLFWmonitor* monitor, GLFWwindow* share, std::string* error)
{
	auto glfwWnd = glfwCreateWindow(width, height, title.c_str(), monitor, share);
	if ( glfwWnd == NULL ) 
	{
		*error = "Failed to create GLFW window";
		return nullptr;
	}

	auto window = new GlfwWindow(glfwWnd);
	window->_width = width;
	window->_height = height;
	window->_title = title;
	glfwSetFramebufferSizeCallback(glfwWnd, userResizeWindowHandler);
	return window;
}


void GlfwWindow::SetContextToWnd(GlfwWindow* wnd)
{
	glfwMakeContextCurrent(wnd->_window);
}


void GlfwWindow::SwapBuffers(const GlfwWindow* window)
{
	glfwSwapBuffers(window->_window);
}


void GlfwWindow::userResizeWindowHandler(GLFWwindow* window, int width, 
	int height)
{
	glViewport(0, 0, width, height);
}




int GlfwWindow::Width() const
{
	return this->_width;
}

int GlfwWindow::Height() const
{
	return this->_height;
}

const GLFWwindow* GlfwWindow::Window() const
{
	return _window;
}

bool GlfwWindow::IsClosing() const
{
	return glfwWindowShouldClose(this->_window);
}

void GlfwWindow::processInput()
{
	if ( glfwGetKey(this->_window, GLFW_KEY_ESCAPE) == GLFW_PRESS )
		glfwSetWindowShouldClose(this->_window, true);
	else
	{
		for ( auto iter : this->_keyBindings )
			if ( glfwGetKey(this->_window, iter.first) == GLFW_PRESS )
				iter.second();
	}
}

void GlfwWindow::BindKey(const int& key, ProcessKeyDelegate callback)
{
	_keyBindings[key] = callback;
}
