#pragma once

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <vector>
#include "Shader.h"


class ShaderProgram
{
private:
	int _id;
	std::vector<int> _shaders;


private:
	ShaderProgram(const int& id);

public:
	~ShaderProgram();

	static ShaderProgram* Init();
	static ShaderProgram* Init(const Shader* vertexShader, const Shader* fragmentShader, 
		std::string* error);
	static ShaderProgram* InitAndDeleteShaders(Shader* vertexShader, 
		Shader* fragmentShader, std::string* error);
	static void UseProgram(const ShaderProgram* prog);

	void AttachShader(const Shader* shader);
	bool LinkProgram(std::string* error);
	bool SetFloat(const std::string& name, const float& val);
	bool SetInt(const std::string& name, const int& val);
	int Id() const;

};

